import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class AnimalService {

  constructor(private Http: HttpClient) {}

  getAllAnimals(): Observable<any> {
    return this.Http.get('//localhost:8080/animal');
  }
}
