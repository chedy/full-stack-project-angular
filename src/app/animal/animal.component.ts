import { Component, OnInit } from '@angular/core';
import {AnimalService} from '../animal.service';

@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.css']
})
export class AnimalComponent implements OnInit {

  Animals: Array<any>

  constructor(private animService: AnimalService) { }

  ngOnInit() {

    this.Animals = [];
    this.animService.getAllAnimals().
    subscribe(data=>{this.Animals=data;});
  }

}
